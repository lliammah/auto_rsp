# Камень, Ножницы, Бумага.
Скрипт игра **"Камень, ножницы, бумага"** для программы [RutonyChat][RutonyChat] для зрителей.


## Возможности скрипта:
* Команды можно указывать на двух языках (русский и английский). Влияет на язык в сообщении ответа.
* Возможность изменить в настройках минимальную стоимость участия и сумму награды.
* В данной версии реализована возможность игрокам менять ставку. Влияет на дополнительное вознаграждение при победе. Дополнительное вознаграждение рассчитывается так: Всё, что было поставлено сверх минимальной стоимость умножается на 2.
* Добавлена возможность настроить множитель для персональной награды. _Минимум 2, максимум 100._
* Данные каждого игрока по игре храняться пока не будет одного из победителей (игрок или компьютер).
* Игнорирование сообщений от _Ботов_. Список _Ботов_ можно отредактировать в настройках. Боты в игры не играют.
* Игнорирование сообщений от _Админов_ (опционально). Список _Админов_ можно отредактировать в настройках.
* Чёрный список ников. Если не хотите, чтобы какой-нибудь зритель играл. Можно править в настройках.
* Изменять количество необходимых побед для завершения игры. _Минимум 3, максимум 10._
* Добавлена возможность настроить количество побед для завершения игры.
* Воспроизведение мелодии при победе и проигрыше.


## Доступные команды:
**!камень** (_rock_) - Игрок делает свой выбор "Камень".

**!ножницы** (_scissors_) - Игрок делает свой выбор "Ножницы".

**!бумага** (_papar_) - Игрок делает свой выбор "Бумага".
> Регистр команд роли не играет, можно писать как угодно, хоть: **!КаМеНь**, **!ножНИЦЫ**, **!БУмаГА**.


## Минимальная ставка и вознаграждение:
Минимальная ставка установлена в 50 кредитов. Это легко можно исправить в настройках. Вознаграждение за победу установлено в 1000 кредитов, так же можно изменить. Если включить возможность игрокам изменять стоимость ставки, то в случае победы они получат дополнительное вознаграждение.


* * *
Если возникнут пожелания или вопросы, пишите [тут][RSP_issues]. Посмотрим, возможно действительно будет что-то интересное.

* * *
### Просмотреть все [мои работы в мастерской][myworkshopfiles].

* * *
### Как можно отблагодарить:
* Оформить удобную для вас подписку на [Boosty.to][Boosty]
* Разово поддержать через [DonationAlerts][DA]

[//]: # (Short links)

   [RutonyChat]: <https://store.steampowered.com/app/524660/RutonyChat/>
   [myworkshopfiles]: <https://steamcommunity.com/id/LLIaMMaH/myworkshopfiles/>
   [RSP_issues]: <https://bitbucket.org/lliammah/auto_rsp/issues>
   [Boosty]: <https://boosty.to/lliammah>
   [DA]: <https://www.donationalerts.com/r/lliammah>