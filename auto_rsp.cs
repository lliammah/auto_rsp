using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Threading;
using System.Linq;
using System.IO;
using Newtonsoft.Json;
using System.Drawing;
using NAudio.Wave;


/*****************************************************************************************
 * 09/03/2022 - 0.3 - Добавлена возможность настраивать множитель для персональной
 *                    награды.
 * 08/03/2022 - 0.2 - Добавлена возмежность настроить количество побед для завершения
 *                    игры. Воспроизведение мелодии при победе и проигрыше.
 * 21/02/2022 - 0.1 - Игра "Камень, ножницы, бумага".
*****************************************************************************************/


namespace RutonyChat
{
    public class RSP
    {

        public ScriptInfo si = new ScriptInfo();
        public DebugMode debug = new DebugMode();
        public Config config = new Config();

        private const int lngEN = 1;
        private const int lngRU = 2;
        private int Language = 0;

        public string[] choice_ru = new string[] { "Камень", "Ножницы", "Бумага" };
        public string[] choice_en = new string[] { "Rock", "Scissors", "Paper" };
        public string choise_text = "";
        public string msg_text = "";

        public List<TurnInfo> player = new List<TurnInfo>();
        public TurnInfo pgame = new TurnInfo();
        public RankControl.ChatterRank cr = new RankControl.ChatterRank();
        public string pfile = "";

        public int wins_user = 0;
        public int wins_game = 0;
        public int wins_no = 0;
        public int additional = 0;

        public Random rnd = new Random();

        // Строка из чата с командами и параметрами.
        private string[] _cmdString;

        // Для патернов в регулярках.
        private string _patt = "";

        readonly RegexOptions options = RegexOptions.IgnoreCase | RegexOptions.Singleline | RegexOptions.CultureInvariant;
        Regex regex;
        MatchCollection m_param;

        public void InitParams(Dictionary<string, string> param)
        {
            // Включен или нет режим отладки
            if (param.ContainsKey("DebugMode"))
            {
                try
                {
                    debug.Mode = bool.Parse(param["DebugMode"]);
                }
                catch { }
            }

            // Уровень отладки
            if (param.ContainsKey("DebugLevel"))
            {
                try
                {
                    var sTmp = param["DebugLevel"];
                    if (!string.IsNullOrEmpty(sTmp)) debug.Level = int.Parse(sTmp[0].ToString());
                }
                catch { }
            }

            // БОТЫ
            if (param.ContainsKey("BotList"))
            {
                try
                {
                    config.BotsList = StringSplit(param["BotList"]);
                }
                catch { }
            }

            // АДМИНЫ
            if (param.ContainsKey("CheckAdmin"))
            {
                try { config.CheckAdmin = bool.Parse(param["CheckAdmin"]); }
                catch { }
            }
            // Список ников админов
            if (param.ContainsKey("AdminList"))
            {
                try
                {
                    config.AdminList = StringSplit(param["AdminList"]);
                }
                catch { }
            }

            // ЧЁРНЫЙ СПИСОК
            if (param.ContainsKey("BlackNiks"))
            {
                try
                {
                    config.BlackList = StringSplit(param["BlackNiks"]);
                }
                catch { }
            }

            // Может игрок изменить ставку
            if (param.ContainsKey("UserCost"))
            {
                try
                {
                    config.UserCost = bool.Parse(param["UserCost"]);
                }
                catch { }
            }
            // Множитель для персональной награды
            if (param.ContainsKey("UserFactor"))
            {
                try
                {
                    config.UserFactor = int.Parse(param["UserFactor"]);
                }
                catch { }
            }
            // Минимальная стоимость участия
            if (param.ContainsKey("MinCost"))
            {
                try
                {
                    config.MinCost = int.Parse(param["MinCost"]);
                }
                catch { }
            }

            // Сумма приза
            if (param.ContainsKey("Prize"))
            {
                try
                {
                    config.Prize = int.Parse(param["Prize"]);
                }
                catch { }
            }

            // Максимальное число побед для завершения игры
            if (param.ContainsKey("MaxWins"))
            {
                try
                {
                    config.MaxWins = int.Parse(param["MaxWins"]);
                }
                catch { }
            }

            // Воспроизведение мелодии при победе игрока
            if (param.ContainsKey("PlayWin"))
            {
                try
                {
                    config.PlayWin = bool.Parse(param["PlayWin"]);
                }
                catch { }
            }
            // Громкость проигрывания мелодии
            if (param.ContainsKey("PlayWinVolume"))
            {
                try
                {
                    config.PlayWinVolume = int.Parse(param["PlayWinVolume"]);
                }
                catch { }
            }
            // Воспроизведение мелодии при проигрыше игрока
            if (param.ContainsKey("PlayLoss"))
            {
                try
                {
                    config.PlayLoss = bool.Parse(param["PlayLoss"]);
                }
                catch { }
            }
            // Громкость проигрывания мелодии
            if (param.ContainsKey("PlayLossVolume"))
            {
                try
                {
                    config.PlayLossVolume = int.Parse(param["PlayLossVolume"]);
                }
                catch { }
            }

            ///////////////////////////////////////////////////////////////////////////////
            // Если скрипту нужно хранить или читать какие-нибудь данные
            // Остальные данные для скрипта
            // Если мы работаем из папки воркшопа
            string folder = RutonyBotFunctions.GetScriptDirectory(si.Script);
            // Иначе мы работаем из папки стима
            if (folder == "") folder = ProgramProps.dir_scripts;
            if (!folder.EndsWith("\\")) folder += @"\";
            folder += si.Folder + @"\";

            if (CheckDir(folder + @"Data")) config.FolderData = folder + @"Data\";
            if (CheckDir(folder + @"Players")) config.FolderPlayers = folder + @"Players\";
            if (debug.Mode && debug.Level >= 2)
            {
                RutonyBot.SayToWindow("Рабочая директория: " + config.FolderData, Color.GreenYellow);
                RutonyBot.SayToWindow("Данные по игрокам хранятся в: " + config.FolderPlayers, Color.GreenYellow);
            }
            if (File.Exists(config.FolderData + "win.mp3")) config.WinWave = config.FolderData + "win.mp3";
            if (File.Exists(config.FolderData + "loss.mp3")) config.LossWave = config.FolderData + "loss.mp3";

            RutonyBot.SayToWindow(string.Format(si.Info, si.Name, si.Version, ""));
        }

        public void Closing()
        {
            RutonyBot.SayToWindow(string.Format(si.Info, si.Name, si.Version, "dis"));
        }


        // Заглушка для стандартного метода
        public void NewAlert(string site, string typeEvent, string subplan, string name, string text, float donate, string currency, int qty) { }



        public void NewMessage(string site, string name, string text, bool system)
        {
            if (system) return;
            if (text[0] != '!') return;

            if (IsName(config.BotsList, name, "Проверяем ботов"))
            {
                if (debug.Mode && debug.Level >= 2) RutonyBot.SayToWindow("Bot: " + name);
                return;
            }

            if (!config.CheckAdmin)
            {
                if (IsName(config.AdminList, name, "Проверяем админов"))
                {
                    if (debug.Mode && debug.Level >= 2) RutonyBot.SayToWindow("Админов не обрабатываем (" + name + ")");
                    return;
                }
            }

            if (IsName(config.BlackList, name, "Проверяем чёрный список"))
            {
                if (debug.Mode && debug.Level >= 2) RutonyBot.SayToWindow("Black Nick: " + name);
                return;
            }

            _cmdString = text.Trim().ToLower().Split(' ');

            // Основная команда для скрипта.
            _patt = @"^[!]{1}(?<cmdGame>";
            _patt += GenPattern(choice_ru) + "|";
            _patt += GenPattern(choice_en) + ")";

            regex = new Regex(_patt, options);
            var mOpen = regex.Matches(_cmdString[0]);

            if (debug.Mode && debug.Level >= 2) RutonyBot.SayToWindow("mOpen.Count = " + mOpen.Count.ToString());

            // Не нашли нужной команды - выход
            if (mOpen.Count == 0) return;


            // На каком языке выводить ответы.
            choise_text = _cmdString[0].Replace("!", "").Trim().ToLower();
            if (Array.Exists(choice_en, v => v.ToLower() == choise_text)) Language = lngEN;
            else Language = lngRU;

            pfile = config.FolderPlayers + name.Trim().Replace(" ", "") + ".json";
            LoadData();

            // Запомним выбор игрока
            if (Language == lngEN) pgame.ChoiceUser = Array.FindIndex(choice_en, t => t.IndexOf(choise_text, StringComparison.InvariantCultureIgnoreCase) >= 0);
            else pgame.ChoiceUser = Array.FindIndex(choice_ru, t => t.IndexOf(choise_text, StringComparison.InvariantCultureIgnoreCase) >= 0);

            // Если у нас указана не только основная команда для скрипта.
            if (config.UserCost)
            {
                if (_cmdString.Length > 1)
                {
                    try
                    {
                        pgame.Cost = Convert.ToInt32(_cmdString[1], null);
                        if (pgame.Cost < config.MinCost)
                        {
                            if (Language == lngEN) msg_text = "The rate cannot be less than the minimum. Minimum bet" + config.MinCost + " " + DeclinationString(config.MinCost, "credit", "credit", "credit") + ".";
                            else msg_text = "Ставка не может быть меньше минимальной. Минимальная ставка = " + config.MinCost + " " + DeclinationString(config.MinCost, "кредит", "кредитов", "кредита") + ".";
                            RutonyBot.BotSay(site, msg_text);
                            return;
                        }
                    }
                    catch { }
                }
                else pgame.Cost = config.MinCost;
            }
            else pgame.Cost = config.MinCost;


            // Финансовый вопрос
            cr = RankControl.ListChatters.FindLast(r => r.Nickname == name.ToLower());
            if (cr != null)
            {
                if (cr.CreditsQty < pgame.Cost)
                {
                    if (Language == lngEN) msg_text = name + ", sorry. But you don't have enough credits.";
                    else msg_text = "Извини " + name + ". Но у тебя не хватает кредитов.";
                    RutonyBot.BotSay(site, msg_text);
                    return;
                }
            }
            else
            {
                if (Language == lngEN) msg_text = name + ", sorry. Looks like you dropped in on us not too long ago. Save some credits and then you can try to play.";
                else msg_text = "Извини " + name + ". Видимо ты не давно к нам заскочил. Подкопи кредитов и тогда попробуешь сыграть.";
                RutonyBot.BotSay(site, msg_text);
                return;
            }


            // Игрок свой выбор сделал, теперь очередь компьютера
            pgame.ChoiceGame = rnd.Next(choice_en.Length);

            if (pgame.ChoiceUser != pgame.ChoiceGame)
            {
                if (Language == lngEN) msg_text = name + " Computer chose: " + choice_en[pgame.ChoiceGame];
                else msg_text = name + " Компьютер выбрал: " + choice_ru[pgame.ChoiceGame];
                RutonyBot.BotSay(site, msg_text);
            }

            pgame.WinGame = false;
            pgame.WinUser = false;
            if (pgame.ChoiceUser == pgame.ChoiceGame)
            {
                if (Language == lngEN) msg_text = name + " and the computer have thrown away " + choice_en[pgame.ChoiceUser] + ". This time it's a draw. Try again.";
                else msg_text = name + " и компьютер выбросили " + choice_ru[pgame.ChoiceUser] + ". В этот раз ничья. Пробуй ещё.";
            }
            // Камень
            else if (pgame.ChoiceUser == 0)
            {
                if (pgame.ChoiceGame == 1)
                {
                    if (Language == lngEN) msg_text = "Rock beats scissors! You have won.";
                    else msg_text = "Камень бьет ножницы! Вы выиграли.";
                    pgame.WinUser = true;
                }
                else
                {
                    if (Language == lngEN) msg_text = "Paper warps up rock! You lose.";
                    else msg_text = "Бумага оборачивает камень! Вы проиграли.";
                    pgame.WinGame = true;
                }
            }
            // Ножницы
            else if (pgame.ChoiceUser == 1)
            {
                if (pgame.ChoiceGame == 0)
                {
                    if (Language == lngEN) msg_text = "Rock beats scissors! You lose.";
                    else msg_text = "Камень бьет ножницы! Вы проиграли.";
                    pgame.WinGame = true;
                }
                else
                {
                    if (Language == lngEN) msg_text = "Scissors cut paper! You have won.";
                    else msg_text = "Ножницы режут бумагу! Вы выиграли.";
                    pgame.WinUser = true;
                }
            }
            // Бумага
            else if (pgame.ChoiceUser == 2)
            {
                if (pgame.ChoiceGame == 0)
                {
                    if (Language == lngEN) msg_text = "Paper warps up rock! You have won.";
                    else msg_text = "Бумага оборачивает камень! Вы выиграли.";
                    pgame.WinUser = true;
                }
                else
                {
                    if (Language == lngEN) msg_text = "Scissors cut paper! You lose.";
                    else msg_text = "Ножницы режут бумагу! Вы проиграли.";
                    pgame.WinGame = true;
                }
            }
            else
            {
                if (Language == lngEN) msg_text = "Something went wrong.";
                else msg_text = "Что-то пошло не так.";
            }

            RutonyBot.BotSay(site, msg_text);
            player.Add(new TurnInfo() { ChoiceUser = pgame.ChoiceUser, ChoiceGame = pgame.ChoiceGame, WinUser = pgame.WinUser, WinGame = pgame.WinGame, Cost = pgame.Cost });
            SaveWins();
            cr.CreditsQty -= pgame.Cost;
            RankControl.ListUpdateSQL.Add(cr);

            Wins();

            if (wins_user >= config.MaxWins)
            {
                if (config.PlayWin) PlaySound(config.WinWave, config.PlayWinVolume);
                if (Language == lngEN) msg_text = "And you are lucky " + name + ". Congratulations on your victory. Get your prize " + config.Prize.ToString() + DeclinationString(config.Prize, "credit", "credit", "credit") + ".";
                else msg_text = "А ты счастливчик " + name + ". Поздравляю тебя с победой. Получи свой приз в размере " + config.Prize.ToString() + " " + DeclinationString(config.Prize, "кредит", "кредитов", "кредита") + ".";

                if (additional > 0)
                {
                    if (Language == lngEN) msg_text += " Additionally you get " + additional.ToString() + " " + DeclinationString(additional, "credit", "credit", "credit") + ".";
                    else msg_text += " Дополнительно ты получаешь " + additional.ToString() + " " + DeclinationString(additional, "кредит", "кредитов", "кредита") + ".";
                }
                if (DeleteWins())
                {
                    RutonyBot.BotSay(site, msg_text);
                    cr.CreditsQty += config.Prize + additional;
                    RankControl.ListUpdateSQL.Add(cr);
                }
                else RutonyBot.SayToWindow("Что-то пошло не так и мы не удалили данные игрока " + name + ".");
            }
            else if (wins_game >= config.MaxWins)
            {
                if (config.PlayLoss) PlaySound(config.LossWave, config.PlayLossVolume);
                if (Language == lngEN) msg_text = "Unfortunately " + name + " you lost. Oh well, maybe you'll get lucky next time.";
                else msg_text = "К сожалению " + name + " ты проиграл. Ну ничего, возможно тебе повезёт в следующий раз.";

                if (DeleteWins())
                {
                    RutonyBot.BotSay(site, msg_text);
                }
                else RutonyBot.SayToWindow("Что-то пошло не так и мы не удалили данные игрока " + name + ".");
            }

            if (wins_user >= config.MaxWins || wins_game >= config.MaxWins)
            {
                if (Language == lngEN) msg_text = name + " Wins you: " + wins_user.ToString() + ". Wins computer: " + wins_game.ToString() + ". Non-player games: " + wins_no.ToString() + ".";
                else msg_text = name + " Побед у тебя: " + wins_user.ToString() + ". Побед у компьютера: " + wins_game.ToString() + ". Ничейных игр: " + wins_no.ToString() + ".";
                RutonyBot.BotSay(site, msg_text);
            }

            player.Clear();
        }







        ////////////////////////////////////////////////////////////////////////////////////////////////////
        // Проиграем указанную мелодию
        public void PlaySound(string file, int volume)
        {
            if (file != "")
            {
                try
                {
                    WaveStream mainOutputStream = new Mp3FileReader(file);
                    WaveChannel32 volumeStream = new WaveChannel32(mainOutputStream);

                    WaveOutEvent player = new WaveOutEvent();
                    player.Init(volumeStream);
                    player.Volume = 1 * (float)volume / 100;
                    player.Play();
                }
                catch { }
            }
        }


        ////////////////////////////////////////////////////////////////////////////////////////////////////
        // Собираем строку с паттерном для команд
        public string GenPattern(string[] choices)
        {
            string res = "";
            for (int i = 0; i < choices.Length; i++)
            {
                res += choices[i].ToLower();
                if (i < choices.Length - 1) res += "|";
            }
            return res;
        }


        ////////////////////////////////////////////////////////////////////////////////////////////////////
        // Есть ли ник в нужном нам списке
        public bool IsName(string[] names, string name, string desc = "")
        {
            var n = name.Trim().ToLower();
            if (debug.Mode && debug.Level >= 2)
            {
                RutonyBot.SayToWindow("Что проверяем: " + desc + " / Размер массива: " + names.Length.ToString());
                RutonyBot.SayToWindow("Имя на проверке: " + name);
            }
            if (names.Length == 0)
            {
                return false;
            }
            else
            {
                return names.Any(Nik => string.Equals(n, Nik.Trim().ToLower(), StringComparison.CurrentCultureIgnoreCase));
            }
        }


        ////////////////////////////////////////////////////////////////////////////////////////////////////
        // Поделим строку
        public string[] StringSplit(string str)
        {
            char[] separators = new char[] { ',' };
            return str.Trim().Replace(" ", "").Split(separators, StringSplitOptions.RemoveEmptyEntries);
        }


        ////////////////////////////////////////////////////////////////////////////////////////////////////
        // Проверка наличия нужной нам папки или создание её при возможности
        public bool CheckDir(string dir)
        {
            var result = false;
            try
            {
                if (Directory.Exists(dir)) result = true;
                else
                {
                    DirectoryInfo di = Directory.CreateDirectory(dir);
                    result = true;
                }
            }
            catch (Exception e)
            {
                RutonyBot.SayToWindow("CheckDir: " + e.ToString());
            }
            finally { }

            return result;
        }


        ////////////////////////////////////////////////////////////////////////////////////////////////////
        // Загрузим данные по игроку (если они есть)
        private void LoadData()
        {
            if (File.Exists(pfile))
            {
                string[] filetexts = File.ReadAllLines(pfile);
                player = JsonConvert.DeserializeObject<List<TurnInfo>>(filetexts[0]);
                if (debug.Mode && debug.Level >= 2) RutonyBot.SayToWindow("Загрузили прошлые данные игрока.");
            }
        }


        ////////////////////////////////////////////////////////////////////////////////////////////////////
        // Сохраним результаты игры для пользователя
        private void SaveWins()
        {
            if (DeleteWins())
            {
                string serialized = JsonConvert.SerializeObject(player);
                RutonyBotFunctions.FileAddString(pfile, serialized);
                if (debug.Mode && debug.Level >= 2) RutonyBot.SayToWindow("Сохранили данные игрока.");
            }
            else
            {
                if (debug.Mode && debug.Level >= 2) RutonyBot.SayToWindow("Что-то пошло не так. Не смоги сохранить данные игрока.");
            }
        }


        ////////////////////////////////////////////////////////////////////////////////////////////////////
        // Удалим старые результаты игры для пользователя
        private bool DeleteWins()
        {
            var result = false;
            try
            {
                File.Delete(pfile);
                if (debug.Mode && debug.Level >= 2) RutonyBot.SayToWindow("Удалили прошлые данные игрока.");
                result = true;
            }
            catch { }
            return result;
        }


        ////////////////////////////////////////////////////////////////////////////////////////////////////
        // Максимальное число побед у какой либо стороны
        private void Wins()
        {
            int count = 0;
            wins_user = 0;
            wins_game = 0;
            wins_no = 0;
            additional = 0;

            if (debug.Mode && debug.Level >= 2) RutonyBot.SayToWindow("В массиве у нас " + player.Count().ToString() + " записей.", Color.DarkMagenta);
            foreach (var p in player)
            {
                if (p.WinUser) wins_user++;
                if (p.WinGame) wins_game++;
                if (!p.WinUser && !p.WinGame) wins_no++;
                additional += p.Cost;
                count++;
            }
            if (additional / count > config.MinCost) additional = (additional - config.MinCost * count) * config.UserFactor;
            else additional = 0;
        }


        ////////////////////////////////////////////////////////////////////////////////////////////////////
        // Правильное текстовое окончание
        // s1 - Штук  s2 - Штука s3 - Штуки
        public string DeclinationString(int i, string s1, string s2, string s3)
        {
            string s = "";

            if (i % 10 == 0) s = s2;
            if (i % 10 == 1) s = s1;
            if (i % 10 >= 2 && i % 10 <= 4) s = s3;
            if (i % 100 >= 11 & i % 100 <= 20) s = s3;

            return s;
        }

    }







    ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Данные о попытках угадать число для конкретного участника
    public class TurnInfo
    {
        public int ChoiceUser = -1;   // Что выбрал игрок
        public int ChoiceGame = -1;   // Что выбрала игра
        public bool WinUser = false;  // Выиграл ли игрок
        public bool WinGame = false;  // Выиграл ли компьютер
        public int Cost = 0;          // Игрок может изменить ставку (влияет на выигрыш)
    }


    ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Объединение параметров для удобства
    public class Config
    {
        public string[] BotsList = { };
        public bool CheckAdmin = true;
        public string[] AdminList = { };
        public string[] BlackList = { };
        public bool UserCost = true;
        public int UserFactor = 2;
        public int MinCost = 0;
        public int Prize = 0;
        public int MaxWins = 3;
        public bool PlayWin = true;
        public int PlayWinVolume = 50;
        public bool PlayLoss = true;
        public int PlayLossVolume = 50;

        // Далее идут настройки, вынесенные в конфиг для удобства
        public string FolderData = "";
        public string FolderPlayers = "";
        public string WinWave = "";
        public string LossWave = "";
    }


    ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Режим и уровень отлпдки для вывода информационных сообщений.
    public class DebugMode
    {
        public bool Mode = false;
        public int Level = 0;
    }


    ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Информация по скрипту. Для удобства вынесена в отдельный класс.
    public class ScriptInfo
    {
        public string Info { get; private set; }
        public string Name { get; private set; }
        public string Version { get; private set; }
        public string Folder { get; private set; }
        public string Script { get; private set; }

        public ScriptInfo()
        {
            this.Info = "<span style='color:yellow;'>{0}</span> script, version <span style='color:#00FF00;'>{1}</span> {2}connected";
            this.Name = "Rock, Scissors, Paper";
            this.Version = "0.3";
            this.Folder = "auto_rsp";
            this.Script = "auto_rsp.cs";
        }

    }
}